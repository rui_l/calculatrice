module fr.rl.calculatrice {
    requires javafx.controls;
    requires javafx.fxml;


    opens fr.rl.calculatrice to javafx.fxml;
    exports fr.rl.calculatrice;
}