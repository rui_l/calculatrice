package fr.rl.calculatrice;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class CalculatriceApplication extends Application {
    private Stage stage;
    private ChangeListener<Number> heightChangeListener;
    private ChangeListener<Number> widthChangeListener;
    private static final double RAPPORT_HAUTEUR_SUR_LARGEUR = 1.5;

    private void heightChanged(Number newValue) {
        stage.widthProperty().removeListener(widthChangeListener);
        stage.setWidth(newValue.intValue()/ RAPPORT_HAUTEUR_SUR_LARGEUR);
        stage.widthProperty().addListener(widthChangeListener);
    }

    private void widthChanged(Number newValue) {
        stage.heightProperty().removeListener(heightChangeListener);
        stage.setHeight(newValue.intValue()* RAPPORT_HAUTEUR_SUR_LARGEUR);
        stage.heightProperty().addListener(heightChangeListener);
    }

    @Override
    public void start(Stage stage) throws IOException {
        this.stage = stage;
        FXMLLoader fxmlLoader = new FXMLLoader(CalculatriceApplication.class.getResource("calculatrice-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("calculatrice.css")).toExternalForm());

        this.stage.setTitle("Calculatrice");
        this.stage.setScene(scene);

        this.stage.setMinWidth(180);
        this.stage.setMinHeight(270);
        this.stage.setMaxWidth(600);
        this.stage.setMaxHeight(900);
        this.stage.setWidth(300);
        this.stage.setHeight(450);
        heightChangeListener = (observableValue, oldValue, newValue)  -> heightChanged(newValue);
        widthChangeListener = (observableValue, oldValue, newValue) -> widthChanged(newValue);
        this.stage.heightProperty().addListener(heightChangeListener);
        this.stage.widthProperty().addListener(widthChangeListener);

        this.stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}