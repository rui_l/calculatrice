package fr.rl.calculatrice.metier;

import java.util.Arrays;
import java.util.List;

public class Calculatrice {
    private Operande operandOne = new Operande("0");
    private Operande operandTwo = new Operande("");
    private String operator;
    private boolean justCompute;


    protected static final List<String> CHIFFRES = Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
    protected static final List<String> OPERATEURS = Arrays.asList("+","-","*","/");

    public Calculatrice() {
        initialize();
    }

    private void initialize() {
        operandOne.initialize();
        operator = "";
        operandTwo.initialize();
        justCompute = false;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(operandOne.getString());
        result.append(operator);
        result.append(operandTwo.getString());
        return result.toString();
    }

    public void touche(String s) {
        switch (s) {
            case "." -> ajouterFraction();
            case "=" -> effectuerOperation();
            default -> ajouterTouchesOrdinaires(s);
        }
    }

    private void ajouterTouchesOrdinaires(String s) {
        if (CHIFFRES.contains(s)){
            ajouterChiffre(s);
        }
        if (OPERATEURS.contains(s)) {
            ajouterOperateur(s);
        }
    }

    private void ajouterChiffre(String s) {
        clearError();
        if ( justCompute && operator.isEmpty() ) {
            operandOne.initialize();
            operandTwo.initialize();
            justCompute = false;
        }
        if (operator.isEmpty())
            operandOne.concatenerChiffre(s);
        else
            operandTwo.concatenerChiffre(s);
    }

    private void ajouterOperateur(String s) {
        clearError();
        if (operator.isEmpty()) {
            operator = s;
        }
        else {
            doCalculation(operandOne, operator, operandTwo);
            justCompute = true;
            operandTwo.initialize();
            operator = s;
        }
    }

    private void effectuerOperation() {
        clearError();
        doCalculation(operandOne, operator, operandTwo);
        justCompute = true;
        operandTwo.initialize();
        operator = "";
    }

    private void ajouterFraction() {
        clearError();
        if (justCompute) {
            initialize();
        }
        if ( operator.isEmpty() && operandOne.hasNotFraction() )
            operandOne.concatenerVirgule();
        if ( (!operator.isEmpty()) && operandTwo.hasNotFraction() )
            operandTwo.concatenerVirgule();
    }

    private void doCalculation(Operande operande1, String operateur, Operande operande2) {
        switch (operateur) {
            case "+" -> operande1.add(operande2);
            case "-" -> operande1.subtract(operande2);
            case "*" -> operande1.multiply(operande2);
            case "/" -> operande1.divide(operande2);
        }
    }

    private void clearError() {
        if (hasError()) {
            initialize();
        }
    }

    private boolean hasError() {
        return operandOne.getString().equals("Erreur ! Division par zero ! ") || operandTwo.getString().equals("Erreur ! Division par zero ! ");
    }

}
