package fr.rl.calculatrice.metier;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

public class Operande {
    private String string;
    private BigDecimal bigDecimal;
    private static final int PRECISION_APRES_VIRGULE = 30;
    private static final int PRECISION_DIVISION = 31;
    private final NumberFormat numberFormat = NumberFormat.getInstance(Locale.FRENCH);

    private final String initialString;

    public Operande(String string) {
        this.string = string;
        numberFormat.setMaximumFractionDigits(PRECISION_APRES_VIRGULE);
        if (string.isEmpty())
            bigDecimal = new BigDecimal("0");
        else {
            String stringNotFrenchFormat = string.replace(",", ".");
            this.bigDecimal = new BigDecimal(stringNotFrenchFormat);
        }
        initialString = string;
    }

    public void initialize() {
        this.string = initialString;
        this.bigDecimal = new BigDecimal("0");
    }

    public String getString() {
        return this.string;
    }
    public BigDecimal getBigDecimal() {
        return this.bigDecimal;
    }

    public boolean isEmpty() {
        return this.string.isEmpty();
    }

    public void concatenerChiffre(String s) {
        if (this.string.equals("0"))
            this.string = s;
        else
            this.string=this.string+s;
        this.bigDecimal = myStringtoBigDecimal(this.string);
    }

    public void concatenerVirgule() {
        if (this.string.isEmpty())
            this.string = "0,";
        else
            this.string+=",";
    }

    public boolean hasNotFraction() {
        return (! this.string.replace(",",".").contains("."));
    }

    private String myBigDecimalToString(BigDecimal bigDecimal) {
        return numberFormat.format(bigDecimal);
    }

    private BigDecimal myStringtoBigDecimal(String string) {
        String stringNotFrenchFormat = string.replace(",",".");
        return new BigDecimal(stringNotFrenchFormat);
    }

    public void add(Operande operande2) {
        this.bigDecimal = this.bigDecimal.add(operande2.getBigDecimal());
        this.string = myBigDecimalToString(this.bigDecimal);
    }

    public void subtract(Operande operande2) {
        this.bigDecimal = this.bigDecimal.subtract(operande2.getBigDecimal());
        this.string = myBigDecimalToString(this.bigDecimal);
    }

    public void multiply(Operande operande2) {
        this.bigDecimal = this.bigDecimal.multiply(operande2.getBigDecimal());
        this.string = myBigDecimalToString(this.bigDecimal);
    }

    public void divide(Operande operande2) {
        try {
            this.bigDecimal = this.bigDecimal.divide(operande2.getBigDecimal(), PRECISION_DIVISION, RoundingMode.HALF_UP);
            this.string = myBigDecimalToString(this.bigDecimal);
        }
        catch (ArithmeticException e) {
            this.string = "Erreur ! Division par zero ! ";
        }
    }

}
