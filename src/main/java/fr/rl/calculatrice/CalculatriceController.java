package fr.rl.calculatrice;

import fr.rl.calculatrice.metier.Calculatrice;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.text.ParseException;

public class CalculatriceController {
    @FXML
    private Label afficheur;
    @FXML
    private Calculatrice calculatrice;

    @FXML
    public void initialize() {
        calculatrice = new Calculatrice();
    }

    @FXML
    protected void onClick(Event event) {
        Button button = (Button) event.getSource();
        calculatrice.touche(button.getText());
        afficheur.setText(calculatrice.toString());
    }
}