package fr.rl.calculatrice.metier;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class OperandeTest {
    Operande operandZeroForTest = new Operande("0");
    Operande operandVideForTest = new Operande("");

    @Test
    void getString() {
        assertEquals("0",operandZeroForTest.getString());
    }

    @Test
    void getBigDecimal() {
        assertEquals(new BigDecimal("0"), operandZeroForTest.getBigDecimal());
    }

    @Test
    void isEmpty() {
        assertTrue(operandVideForTest.isEmpty());
    }

    @Test
    void concatenerChiffre() {
        Operande operand = operandZeroForTest;
        operand.concatenerChiffre("9");
        assertEquals("9",operand.getString());
    }

    @Test
    void concatenerVirgule() {
    }

    @Test
    void hasNotFraction() {
    }

    @Test
    void add() {
    }

    @Test
    void subtract() {
    }

    @Test
    void multiply() {
    }

    @Test
    void divide() {
    }
}